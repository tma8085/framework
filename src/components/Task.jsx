import React, { useState } from "react";
import {
  ListItem,
  ListItemIcon,
  Checkbox,
  TextField,
  IconButton,
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";

export default function Task({
  checked,
  text,
  onDelete,
  id,
  updateChecked,
  updateText,
}) {
  const [check, setCheck] = useState(checked);
  const [textValue, setTextValue] = useState(text);

  return (
    <div className="tasks">
      <ListItem>
        <ListItemIcon>
          <Checkbox
            checked={check}
            onChange={() => {
              updateChecked({ text: textValue, checked: !checked, id });
              setCheck(!check);
            }}
          />
        </ListItemIcon>
        <TextField
          className="text"
          value={textValue}
          onChange={(e) => setTextValue(e.target.value)}
          onBlur={() => {
            updateText({ text: textValue, checked: checked, id });
          }}
        />
        <IconButton
          className="close"
          onClick={() => {
            onDelete(id);
          }}
        >
          <CloseIcon />
        </IconButton>
      </ListItem>
    </div>
  );
}
