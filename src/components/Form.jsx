import React from "react";
import { TextField, Button } from "@material-ui/core";
import Task from "./Task";
import postsAPI from "../api/postsAPI";

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: "",
      tasks: [
        { checked: true, text: "Task1" },
        { checked: false, text: "Task2" },
      ],
    };
  }

  componentDidMount() {
    this.getTasks();
  }

  getTasks = () => {
    postsAPI.getList().then((data) => {
      this.setState({ tasks: data });
    });
  };

  addTasks = (e) => {
    e.preventDefault();
    const { text } = this.state;
    postsAPI.addItem(text).then((data) => {
      this.getTasks();
    });
    this.setState({
      text: "",
    });
  };

  editTasks = ({ text, checked, id }) => {
    postsAPI.editItem({ text, checked, id }).then((data) => {});
  };

  deleteTask = (id) => {
    postsAPI.deleteItem(id).then((data) => {
      this.getTasks();
    });
  };

  render() {
    const { tasks, text } = this.state;
    return (
      <div className="wrapper">
        <form onSubmit={this.addTasks} className="wrapAdd">
          <TextField
            className="add textField"
            label="Task"
            variant="outlined"
            size="small"
            value={text}
            onSubmit={this.addTasks}
            onChange={(e) => {
              this.setState({
                text: e.target.value,
              });
            }}
          />
          <Button
            onClick={this.addTasks}
            className="add"
            variant="outlined"
            size="small"
            color="primary"
          >
            Add
          </Button>
        </form>
        {tasks.map((task) => (
          <Task
            key={task.id}
            id={task.id}
            text={task.text}
            checked={task.checked}
            updateChecked={this.editTasks}
            updateText={this.editTasks}
            onDelete={this.deleteTask}
          />
        ))}
      </div>
    );
  }
}
export default Form;
