import Axios from "axios";

const Instance = Axios.create({
  baseURL: process.env.REACT_APP_API_UPL,
});

export default Instance;
